package limamauricio.tvsala.activitys;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import limamauricio.tvsala.R;
import limamauricio.tvsala.helpers.MqttHelper;

public class MainActivity extends AppCompatActivity {

    final String pubTopicStatus = "sensor/tvSala/statusC";
    final String pubTopicVolume = "sensor/tvSala/volumeC";

    MqttHelper mqttHelper;

    SeekBar simpleSeekBar;
    Switch power;

    String statusTV;

    Button publish;

    CameraManager mCameraManager;
    String mCameraId;

    String volumeString;

    TextView status, volume;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        status = (TextView) findViewById(R.id.dataReceived);
        volume = (TextView)findViewById(R.id.volume);
        simpleSeekBar = (SeekBar)findViewById(R.id.seekBar);
        power = (Switch)findViewById(R.id.power);
        publish = (Button)findViewById(R.id.button);

        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            mCameraId = mCameraManager.getCameraIdList()[0];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        simpleSeekBar.setProgress(0);
        volumeString = "0";
        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
//                Toast.makeText(MainActivity.this, "Seek bar progress is :" + progressChangedValue,
//                        Toast.LENGTH_SHORT).show();
                volume.setText(""+ progressChangedValue);
                volumeString = ""+ progressChangedValue;
            }
        });

        final MqttMessage message = new MqttMessage();
        final String[] publishMessage = {""};

        final MqttMessage messageVolume = new MqttMessage();
        final String[] publishMessageVolume = {""};

        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (power.isChecked()){
                    publishMessage[0] = "Ligado";
                    message.setPayload(publishMessage[0].getBytes());

                    publishMessageVolume[0] = volumeString;
                    messageVolume.setPayload(publishMessageVolume[0].getBytes());
                    try {
                        mqttHelper.mqttAndroidClient.publish(pubTopicStatus, message);
                        mqttHelper.mqttAndroidClient.publish(pubTopicVolume, messageVolume);
                        status.setText(message.toString());
                        turnOnLight();
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    publishMessage[0] = "Desligado";
                    message.setPayload(publishMessage[0].getBytes());

                    publishMessageVolume[0] = volumeString;
                    messageVolume.setPayload(publishMessageVolume[0].getBytes());
                    try {
                        mqttHelper.mqttAndroidClient.publish(pubTopicStatus, message);
                        mqttHelper.mqttAndroidClient.publish(pubTopicVolume, messageVolume);
                        status.setText(message.toString());
                        turnOffLight();
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        startMqtt();

    }

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "MQTT CONNECTION LOST");

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                statusTV = mqttMessage.toString();

                if (topic.equals("sensor/c/tvSala/status")) {
                    if (statusTV.matches("Ligado")){
                        status.setText(mqttMessage.toString());
                        power.setChecked(true);
                       turnOnLight();
                    }else {
                        status.setText(mqttMessage.toString());
                        power.setChecked(false);
                        turnOffLight();
                    }

                }
                if (topic.equals("sensor/c/tvSala/volume")) {
                    volume.setText(statusTV);
                    simpleSeekBar.setProgress(Integer.parseInt(statusTV));

                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Debug", "MQTT DELIVERY COMPLETE!");

            }
        });
    }

    public void turnOnLight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Turn off flashlight method
    public void turnOffLight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
