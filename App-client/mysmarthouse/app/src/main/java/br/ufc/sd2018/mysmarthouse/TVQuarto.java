package br.ufc.sd2018.mysmarthouse;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.ufc.sd2018.mysmarthouse.helpers.MqttHelper;

public class TVQuarto extends AppCompatActivity {

    final String pubTopicStatus = "sensor/c/tvQuarto/status";
    final String pubTopicVolume = "sensor/c/tvQuarto/volume";

    Switch power_tvQuarto;
    MqttHelper mqttHelper;
    CameraManager mCameraManager;
    String mCameraId;
    TextView statusTvQuarto, volumeData;
    String statusString, volumeString;
    SeekBar simpleSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvquarto);


        startMqtt();

        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            mCameraId = mCameraManager.getCameraIdList()[0];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        power_tvQuarto = (Switch)findViewById(R.id.power_tvQuarto);
        statusTvQuarto = (TextView)findViewById(R.id.statusTVQuarto);
        simpleSeekBar = (SeekBar) findViewById(R.id.seekBarVolumeTvQuarto);
        volumeData = (TextView)findViewById(R.id.volumeTvQuarto);

        volumeData.setText("0");
        simpleSeekBar.setProgress(0);
        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
//                Toast.makeText(MainActivity.this, "Seek bar progress is :" + progressChangedValue,
//                        Toast.LENGTH_SHORT).show();
                volumeData.setText(""+ progressChangedValue);
                volumeString = ""+ progressChangedValue;
            }
        });


        Button alterar = (Button)findViewById(R.id.buttonAlterarTvQuarto);
        final MqttMessage message = new MqttMessage();
        final String[] publishMessage = {""};

        final MqttMessage messageVolume = new MqttMessage();
        final String[] publishMessageVolume = {""};

        alterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (power_tvQuarto.isChecked()){
                    publishMessage[0] = "Ligado";
                    message.setPayload(publishMessage[0].getBytes());


                    publishMessageVolume[0] = volumeString;
                    messageVolume.setPayload(publishMessageVolume[0].getBytes());

                    try {
                        mqttHelper.mqttAndroidClient.publish(pubTopicStatus, message);
                        mqttHelper.mqttAndroidClient.publish(pubTopicVolume, messageVolume);
                        statusTvQuarto.setText(message.toString());
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    publishMessage[0] = "Desligado";
                    message.setPayload(publishMessage[0].getBytes());


                    publishMessageVolume[0] = volumeString;
                    messageVolume.setPayload(publishMessageVolume[0].getBytes());

                    try {
                        mqttHelper.mqttAndroidClient.publish(pubTopicStatus, message);
                        mqttHelper.mqttAndroidClient.publish(pubTopicVolume, messageVolume);
                        statusTvQuarto.setText(message.toString());
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void turnOnLight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Turn off flashlight method
    public void turnOffLight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "MQTT CONNECTION LOST");

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                // statusArCond.setText(mqttMessage.toString());

                statusString = mqttMessage.toString();

                // switch (topic)
                if (topic.equals("sensor/tvSala/statusC")) {
                    //sensorLuminosidadeStatus.setText(mqttMessage.toString());
                    if (statusString.matches("Ligado")){
                        statusTvQuarto.setText(mqttMessage.toString());
                        power_tvQuarto.setChecked(true);
                        //turnOnLight();
                    }else {
                        statusTvQuarto.setText(mqttMessage.toString());
                        power_tvQuarto.setChecked(false);
                        //turnOffLight();
                    }

                }if (topic.equals("sensor/tvSala/statusC")) {
                    volumeData.setText(statusString);
                    simpleSeekBar.setProgress(Integer.parseInt(statusString));
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Debug", "MQTT DELIVERY COMPLETE!");

            }
        });
    }

}
