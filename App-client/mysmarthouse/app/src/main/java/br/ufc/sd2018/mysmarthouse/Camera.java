package br.ufc.sd2018.mysmarthouse;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.ufc.sd2018.mysmarthouse.helpers.MqttHelper;

public class Camera extends AppCompatActivity {

    final String pubTopicStatus = "sensor/c/cam/status";

    Switch power_luz;
    MqttHelper mqttHelper;
    CameraManager mCameraManager;
    String mCameraId;
    TextView statusLuz;
    String statusLuzString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);


        startMqtt();

        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            mCameraId = mCameraManager.getCameraIdList()[0];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        power_luz = (Switch)findViewById(R.id.switch_power_cam);
        statusLuz = (TextView)findViewById(R.id.txt_status_camera);

        Button alterar = (Button)findViewById(R.id.btn_change_cam);
        final MqttMessage message = new MqttMessage();
        final String[] publishMessage = {""};


        alterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (power_luz.isChecked()){
                    publishMessage[0] = "Ligado";
                    message.setPayload(publishMessage[0].getBytes());


                    try {
                        mqttHelper.mqttAndroidClient.publish(pubTopicStatus, message);
                        statusLuz.setText(message.toString());
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    publishMessage[0] = "Desligado";
                    message.setPayload(publishMessage[0].getBytes());


                    try {
                        mqttHelper.mqttAndroidClient.publish(pubTopicStatus, message);
                        statusLuz.setText(message.toString());
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void turnOnLight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Turn off flashlight method
    public void turnOffLight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "MQTT CONNECTION LOST");

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                // statusArCond.setText(mqttMessage.toString());

                statusLuzString = mqttMessage.toString();

                // switch (topic)
                if (topic.equals("sensor/cam/statusC")) {
                    //sensorLuminosidadeStatus.setText(mqttMessage.toString());
                    if (statusLuzString.matches("Ligado")){
                        statusLuz.setText(mqttMessage.toString());
                        power_luz.setChecked(true);
                        //turnOnLight();
                    }else {
                        statusLuz.setText(mqttMessage.toString());
                        power_luz.setChecked(false);
                       // turnOffLight();
                    }

                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Debug", "MQTT DELIVERY COMPLETE!");

            }
        });
    }

    private void alterarVolume(final String vol) {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.w("Debug", "MQTT CONNECTED");

//                MqttMessage myMess = new MqttMessage(vol.getBytes());
//                myMess.setQos(1);
//                myMess.setRetained(true);
//                try {
//                    mqttHelper.publish(pubTopicVolume,myMess);
//                } catch (MqttException e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "MQTT CONNECTION LOST");

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                // statusLuzString.setText(mqttMessage.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Debug", "MQTT DELIVERY COMPLETE!");

            }
        });
    }

}

