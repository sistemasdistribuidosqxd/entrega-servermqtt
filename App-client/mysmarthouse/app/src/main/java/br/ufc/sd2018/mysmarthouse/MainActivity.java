package br.ufc.sd2018.mysmarthouse;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import java.io.UnsupportedEncodingException;

import br.ufc.sd2018.mysmarthouse.helpers.MqttHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView sensorLuminosidadeStatus;
    String statusString;

    CardView lumn, light, cam, TVQuarto, TVSala;

    MqttHelper mqttHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorLuminosidadeStatus = (TextView) findViewById(R.id.txt_luminosidade);

        lumn = (CardView) findViewById(R.id.card_lum);
        light = (CardView) findViewById(R.id.card_light);
        cam = (CardView) findViewById(R.id.card_cam);
        TVQuarto = (CardView) findViewById(R.id.card_tv_quarto);
        TVSala = (CardView) findViewById(R.id.card_tv_sala);

        lumn.setOnClickListener(this);
        light.setOnClickListener(this);
        cam.setOnClickListener(this);
        TVQuarto.setOnClickListener(this);
        TVSala.setOnClickListener(this);

        startMqtt();
    }


    @Override
    public void onClick(View view) {
        Intent i = null;

        switch (view.getId()) {
            case R.id.card_lum:
                i = new Intent(this, Luminosidade.class);
                startActivity(i);
                break;
            case R.id.card_light:
                i = new Intent(this, Light.class);
                startActivity(i);
                break;
            case R.id.card_cam:
                i = new Intent(this, Camera.class);
                startActivity(i);
                break;
            case R.id.card_tv_quarto:
                i = new Intent(this, br.ufc.sd2018.mysmarthouse.TVQuarto.class);
                startActivity(i);
                break;
            case R.id.card_tv_sala:
                i = new Intent(this, br.ufc.sd2018.mysmarthouse.TVSala.class);
                startActivity(i);
                break;
            default:
                return;
        }


    }

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "MQTT CONNECTION LOST");
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());

                statusString = mqttMessage.toString();

                // switch (topic)
                if (topic.equals("sensor/luminosidade")) {
                    sensorLuminosidadeStatus.setText(mqttMessage.toString());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Debug", "MQTT DELIVERY COMPLETE!");
            }
        });
    }
}



