package br.ufc.sd2018.mysmarthouse;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;
import android.hardware.camera2.*;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.ufc.sd2018.mysmarthouse.helpers.ChartHelper;
import br.ufc.sd2018.mysmarthouse.helpers.MqttHelper;

public class Luminosidade extends AppCompatActivity {

    MqttHelper mqttHelper;
    TextView ultimoDado;

    ChartHelper mChart;
    LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_luminosidade);

        startMqtt();

        ultimoDado = (TextView)findViewById(R.id.ultimodado);
        chart = (LineChart) findViewById(R.id.chart);
        mChart = new ChartHelper(chart);
    }

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "MQTT CONNECTION LOST");

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                if (topic.equals("sensor/luminosidade")) {
                    ultimoDado.setText(mqttMessage.toString());
                    mChart.addEntry(Float.valueOf(mqttMessage.toString()));
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Debug", "MQTT DELIVERY COMPLETE!");
            }
        });
    }
}
